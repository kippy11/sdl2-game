#include "SoundEffects.h"
#include <iostream>
#include "Game.h"
#include "SDLInit.h"

SoundEffects::SoundEffects()
{
    SDLInit::initSDLSound();
}

void SoundEffects::addSoundEffect(const char* path)
{
    Mix_Chunk* tmpChunk = Mix_LoadWAV(path);

    if (tmpChunk != nullptr)
    {
        MSound.push_back(tmpChunk);
    }
    else
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't init audio: %s", Mix_GetError());
    }
}

void SoundEffects::playSoundEffect(const int which) const
{
    if (which > MSound.size() + 1)
    {
        return;
    }

    Mix_PlayChannel(- 1, MSound[which], 0);

}
