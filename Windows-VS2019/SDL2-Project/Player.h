#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;
class Game;

class Player : public Sprite
{
private:
    
    // Animation state
    int state;
    int weaponState = 1;
    int pHealth;
        
    // Sprite information
    static const int SPRITE_HEIGHT = 32;
    static const int SPRITE_WIDTH = 24;

    // Bullet spawn
    float cooldownTimer;
    static const float COOLDOWN_MAX;


    bool Dead = false;

    Game* game;

    // Score
    int points;
public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT=0, RIGHT, UP, DOWN, IDLE, DEAD};
    
    void init(SDL_Renderer *renderer);
    void processInput(const Uint8 *keyStates);
    void takeDamage(int damage);
    void isDead();

    void setGame(Game* game);
    void fire();
    void shoot();

    int getCurrentAnimationState();

    //Overloads
    void update(float timeDeltaInSeconds);

    void addScore(int points);
    int getScore();
};



#endif