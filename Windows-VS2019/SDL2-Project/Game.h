#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"

#include <vector>
using std::vector;

class Player;
class NPC;
class Bullet;
class Arrow;
class Vector2f;

class Game
{
private:
    // Declare window and renderer objects
    SDL_Window* gameWindow;
    SDL_Renderer* gameRenderer;
    

    SDL_Color* color;
    SDL_Surface* susurfaceMessage;
    SDL_Texture* Message;


    // Background texture
    SDL_Texture* backgroundTexture;

    //Declare Player
    Player* player;

    //Declare NPC
    NPC* npc;
    vector<NPC*> npc1;
    // Bullets
    vector<Bullet*> bullets;
    vector<Arrow*> arrows;

    // Window control 
    bool            quit;

    // Keyboard
    const Uint8* keyStates;


    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();

public:
    // Constructor 
    Game();
    ~Game();

    // Methods
    void init();
    void runGameLoop();
    Player* getPlayer();
    void createBullet(Vector2f* position, Vector2f* velocity);
    void createArrow(Vector2f* position, Vector2f* velocity);
    void collisionDetection();
    void RenderHPBar(int x, int y, int w, int h, float Percent, SDL_Color FGColor, SDL_Color BGColor);

    // Public attributes
    static const int WINDOW_WIDTH = 800;
    static const int WINDOW_HEIGHT = 600;

    int points;
};

#endif