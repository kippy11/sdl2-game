#include "Music.h"
#include "Game.h"
#include "SDLInit.h"
#include <SDL.h>

Music::Music()
{
    SDLInit::initSDLSound();
}



void Music::addMusicTrack(const char* path)
{
    Mix_Music* tmp_music = Mix_LoadMUS(path);

    if (tmp_music != nullptr)
    {
        MMusic.push_back(tmp_music);
    }
    else
    {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
            "Couldn't load audio: %s",
            Mix_GetError());
    }
}

void Music::playMusicTrack(const int which) 
{
    if (which > MMusic.size())
    {
        return;
    }

    Mix_PlayMusic(MMusic.at(which), -1);

    MPlaying = true;

}

