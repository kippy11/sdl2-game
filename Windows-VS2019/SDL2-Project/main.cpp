/*
  Moving Game into it's own structure / files.  
*/

//For exit()
#include <cstdlib>
#include<iostream>
//for printf
#include <cstdio>

#include "SDL2Common.h"
#include "Game.h"
#include "Music.h"
#include "SoundEffects.h"
#include <SDL_ttf.h>
#include <string>
using namespace std;

const int SDL_OK = 0;

int main(int argc, char* args[] )
{

    
    // SDL allows us to choose which SDL components are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

     

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }
    

    Game *game = new Game();

    game->init();


    Music music;
    music.addMusicTrack("assets/sounds/End.wav");
    music.addMusicTrack("assets/sounds/Level2.wav");
    music.playMusicTrack(0);

    /*SoundEffects se;
    se.addSoundEffect("assets/sounds.shoot.wav");
    se.addSoundEffect("assets/sounds.shoot.mp3");
    se.playSoundEffect(0);*/

    
    
    
    game->runGameLoop();

    delete game;

    //Shutdown SDL - clear up resources etc.
    SDL_Quit();

    exit(0);

    return(0);
}

void points()
{
    SDL_Renderer* renderer{};
    SDL_Surface* surface{};

    int fontsize = 24;
    int t_width = 0; // width of the loaded font-texture
    int t_height = 0; // height of the loaded font-texture
    SDL_Color text_color = { 0,0,0 };
    string fontpath = "assets/font/Sans.ttf";
    string text = "points: ";
    TTF_Font* font = TTF_OpenFont(fontpath.c_str(), fontsize);
    SDL_Texture* ftexture = NULL; // our font-texture

    // check to see that the font was loaded correctly
    if (font == NULL) {
        cerr << "Failed the load the font!\n";
        cerr << "SDL_TTF Error: " << TTF_GetError() << "\n";
    }
    else {
        // now create a surface from the font
        SDL_Surface* text_surface = TTF_RenderText_Solid(font, text.c_str(), text_color);

        // render the text surface
        if (text_surface == NULL) {
            cerr << "Failed to render text surface!\n";
            cerr << "SDL_TTF Error: " << TTF_GetError() << "\n";
        }
        else {
            // create a texture from the surface
            ftexture = SDL_CreateTextureFromSurface(renderer, text_surface);

            if (ftexture == NULL) {
                cerr << "Unable to create texture from rendered text!\n";
            }
            else {
                t_width = text_surface->w; // assign the width of the texture
                t_height = text_surface->h; // assign the height of the texture

                // clean up after ourselves (destroy the surface)
                SDL_FreeSurface(surface);
            }
        }
    }
}