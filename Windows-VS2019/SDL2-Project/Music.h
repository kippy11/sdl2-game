#ifndef MSUIC_H_
#define MUSIC_H_

#include "Game.h"
#include "TextureUtils.h"
#include "SDL2Common.h"
#include<vector>
#include <iostream>
#pragma once
class Music
{
public:

	Music();

	void addMusicTrack(const char* path);
	void playMusicTrack(const int which);
	void Play_Pause();

private:

	std::vector<Mix_Music*> MMusic;
	bool MPaused = false;

	bool MPlaying = false;
};

#endif
