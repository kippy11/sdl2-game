#ifndef SOUNDEFFECTS_H_
#define SOUNDEFFECTS_H_

#pragma once
#include "SDL2Common.h"
#include <vector>
class SoundEffects
{
public:
	SoundEffects();

	void addSoundEffect(const char* path);

	void playSoundEffect(const int which) const;

private:

	std::vector<Mix_Chunk*> MSound;

};

#endif